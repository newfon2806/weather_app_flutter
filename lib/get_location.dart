import 'package:flutter/material.dart';
import 'package:location/location.dart';

class GetLocationWidget extends StatefulWidget {
  GetLocationWidget({Key key}) : super(key: key);

  @override
  _GetLocationState createState() => _GetLocationState();
}

class _GetLocationState extends State<GetLocationWidget> {
  final Location location = new Location();

  LocationData _locationData;

  _getLocation() async {
    _locationData = await location.getLocation();
  }

  @override
  void initState() {
    super.initState();
    _getLocation();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Location: ' + ('${_locationData ?? "unknown"}'),
          style: Theme.of(context).textTheme.body2,
        ),
      ],
    );
  }
}