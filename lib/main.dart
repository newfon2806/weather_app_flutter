import 'package:flutter/material.dart';
import 'package:weather_app/homepage.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  final String appTitle = "Weather App";
  // final Color primaryColor = Color(0xff242630);
  final Color primaryColor = Color(0xffe9eae8);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: appTitle,
      theme: new ThemeData(
        textTheme: TextTheme(
          body1: TextStyle(color: Colors.black)
        ),
        canvasColor: Color(0xffe9eae8)
      ),
      home: MyHomePage(title: appTitle, primaryColor: primaryColor),
    );
  }
}

