class Post {
  final String name;
  final Main temp;
  final WeatherList main;
  final WeatherList description;
  final WeatherList icon;

  Post({this.name, this.temp, this.main, this.description, this.icon});

  factory Post.fromJson(Map<String, dynamic> json) {
    return Post(
        name: json['name'],
        temp: Main.fromJson(json['main']),
        main: WeatherList.fromJson(json['weather']),
        description: WeatherList.fromJson(json['weather']),
        icon: WeatherList.fromJson(json['weather']),
      );
  }
}

class Main {
  var temp;
  var pressure;
  var humidity;

  Main({this.temp, this.humidity, this.pressure});

  factory Main.fromJson(Map<String, dynamic> json) {
    return Main(
        temp: json['temp'],
        pressure: json['pressure'],
        humidity: json['humidity']);
  }
}

class Weather {
  String main;
  String description;
  String icon;

  Weather({this.main, this.description, this.icon});

  factory Weather.fromJson(Map<String, dynamic> json) {
    return new Weather(
        main: json['main'],
        description: json['description'],
        icon: json['icon']);
  }
}

class WeatherList {
  List<Weather> weather;

  WeatherList({
    this.weather,
  });

  factory WeatherList.fromJson(List<dynamic> json) {

    List<Weather> weather = new List<Weather>();
    weather = json.map((i)=>Weather.fromJson(i)).toList();

    return new WeatherList(
       weather: weather,
    );
  }
}
