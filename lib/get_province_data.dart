import 'package:flutter/material.dart';
import 'post.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:division/division.dart';

class HttpServiceProvince {
  String postsURL;
  String manyPostsURL;

  final province = [
    {"name": "เชียงราย", "id": 1153668, "zone": "north"},
    {"name": "เชียงใหม่", "id": 1153671, "zone": "north"},
    {"name": "น่าน", "id": 1608451, "zone": "north"},
    {"name": "พะเยา", "id": 1607758, "zone": "north"},
    {"name": "แพร่", "id": 1607551, "zone": "north"},
    {"name": "แม่ฮ่องสอน", "id": 1152221, "zone": "north"},
    {"name": "ลำปาง", "id": 1152472, "zone": "north"},
    {"name": "ลำพูน", "id": 1152467, "zone": "north"},
    {"name": "อุตรดิตถ์", "id": 1605214, "zone": "north"},
    {"name": "กาฬสินธุ์", "id": 1610468, "zone": "north-east"},
    {"name": "ขอนแก่น", "id": 1609775, "zone": "north-east"},
    {"name": "ชัยภูมิ", "id": 1611406, "zone": "north-east"},
    {"name": "นครพนม", "id": 1608530, "zone": "north-east"},
    {"name": "นครราชสีมา", "id": 1608528, "zone": "north-east"},
    {"name": "บึงกาฬ", "id": 8133594, "zone": "north-east"},
    {"name": "บุรีรัมย์", "id": 1611452, "zone": "north-east"},
    {"name": "มหาสารคาม", "id": 1608899, "zone": "north-east"},
    {"name": "มุกดาหาร", "id": 1608595, "zone": "north-east"},
    {"name": "ยโสธร", "id": 1604767, "zone": "north-east"},
    {"name": "ร้อยเอ็ด", "id": 1607000, "zone": "north-east"},
    {"name": "เลย", "id": 1609070, "zone": "north-east"},
    {"name": "ศรีสะเกษ", "id": 1606239, "zone": "north-east"},
    {"name": "สกลนคร", "id": 1606789, "zone": "north-east"},
    {"name": "สุรินทร์", "id": 1606029, "zone": "north-east"},
    {"name": "หนองคาย", "id": 1608231, "zone": "north-east"},
    {"name": "หนองบัวลำภู", "id": 1608269, "zone": "north-east"},
    {"name": "อุดรธานี", "id": 1906686, "zone": "north-east"},
    {"name": "อุบลราชธานี", "id": 1906688, "zone": "north-east"},
    {"name": "อำนาจเจริญ", "id": 1906689, "zone": "north-east"},
    {"name": "กรุงเทพมหานคร", "id": 1609350, "zone": "center"},
    {"name": "กำแพงเพชร", "id": 1153089, "zone": "center"},
    {"name": "ชัยนาท", "id": 1611415, "zone": "center"},
    {"name": "นครนายก", "id": 1608538, "zone": "center"},
    {"name": "นครปฐม", "id": 1608533, "zone": "center"},
    {"name": "นครสวรรค์", "id": 1608526, "zone": "center"},
    {"name": "นนทบุรี", "id": 1608132, "zone": "center"},
    {"name": "ปทุมธานี", "id": 1607982, "zone": "center"},
    {"name": "พระนครศรีอยุธยา", "id": 1607530, "zone": "center"},
    {"name": "พิจิตร", "id": 1607724, "zone": "center"},
    {"name": "พิษณุโลก", "id": 1607707, "zone": "center"},
    {"name": "เพชรบูรณ์", "id": 1607736, "zone": "center"},
    {"name": "ลพบุรี", "id": 1609031, "zone": "center"},
    {"name": "สมุทรปราการ", "id": 1606589, "zone": "center"},
    {"name": "สมุทรสงคราม", "id": 1606585, "zone": "center"},
    {"name": "สมุทรสาคร", "id": 1606587, "zone": "center"},
    {"name": "สระบุรี", "id": 1606417, "zone": "center"},
    {"name": "สิงห์บุรี", "id": 1606269, "zone": "center"},
    {"name": "สุโขทัย", "id": 1150532, "zone": "center"},
    {"name": "สุพรรณบุรี", "id": 1606032, "zone": "center"},
    {"name": "อ่างทอง", "id": 1621034, "zone": "center"},
    {"name": "อุทัยธานี", "id": 1149965, "zone": "center"},
    {"name": "จันทบุรี", "id": 1611268, "zone": "east"},
    {"name": "ฉะเชิงเทรา", "id": 1611438, "zone": "east"},
    {"name": "ชลบุรี", "id": 1611108, "zone": "east"},
    {"name": "ตราด", "id": 1605277, "zone": "east"},
    {"name": "ปราจีนบุรี", "id": 1906687, "zone": "east"},
    {"name": "ระยอง", "id": 1607016, "zone": "east"},
    {"name": "สระแก้ว", "id": 1906691, "zone": "east"},
    {"name": "กาญจนบุรี", "id": 1153080, "zone": "west"},
    {"name": "ตาก", "id": 1150489, "zone": "west"},
    {"name": "ประจวบคีรีขันธ์", "id": 1151073, "zone": "west"},
    {"name": "เพชรบุรี", "id": 1151416, "zone": "west"},
    {"name": "ราชบุรี", "id": 1150953, "zone": "west"},
    {"name": "กระบี่", "id": 1152631, "zone": "south"},
    {"name": "ชุมพร", "id": 1153555, "zone": "south"},
    {"name": "ตรัง", "id": 1150006, "zone": "south"},
    {"name": "นครศรีธรรมราช", "id": 1608525, "zone": "south"},
    {"name": "นราธิวาส", "id": 1608408, "zone": "south"},
    {"name": "ปัตตานี", "id": 1607976, "zone": "south"},
    {"name": "พังงา", "id": 1151464, "zone": "south"},
    {"name": "พัทลุง", "id": 1607778, "zone": "south"},
    {"name": "ภูเก็ต", "id": 1151253, "zone": "south"},
    {"name": "ยะลา", "id": 1604869, "zone": "south"},
    {"name": "ระนอง", "id": 1150964, "zone": "south"},
    {"name": "สตูล", "id": 1606375, "zone": "south"},
    {"name": "สงขลา", "id": 1606146, "zone": "south"},
    {"name": "สุราษฎร์ธานี", "id": 1150514, "zone": "south"}
  ];

  final String apiKey = "e5bc2f8dd799d09bc4fddfd07256be57";

  // Future<Post> getManyPost() async {
  Future<List<Post>> getManyPost() async {
    List<Post> provinceData = [];
    for (int i = 0; i < this.province.length; i++) {
      var item = this.province[i];
      manyPostsURL = "https://api.openweathermap.org/data/2.5/weather?id=" +
          item["id"].toString() +
          "&appid=" +
          apiKey;
      final http.Response res = await http.get(this.manyPostsURL);
      if (res.statusCode == 200) {
        provinceData.add(Post.fromJson(json.decode(res.body)));
      } else {
        print("cannot get data");
        throw Exception('Failed to load');
      }
      print(i);
    }

    return provinceData;
  }
}

class GetProvinceDataWidget extends StatefulWidget {
  GetProvinceDataWidget({Key key}) : super(key: key);

  @override
  _GetProvinceDataState createState() => _GetProvinceDataState();
}

class _GetProvinceDataState extends State<GetProvinceDataWidget> {
  HttpServiceProvince httpService = new HttpServiceProvince();
  Future<Post> futurePost;
  Future<List<Post>> listFuturePost;

  @override
  void initState() {
    super.initState();
    listFuturePost = httpService.getManyPost();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Post>>(
      future: listFuturePost,
      builder: (context, AsyncSnapshot<List> snapshot) {
        if (snapshot.hasData) {
          // return Text(snapshot.data[0].icon.weather[0].icon.toString());
          return _getHeroCarosuel(snapshot);
        } else if (snapshot.hasError) {
          return Text("${snapshot.error}");
        }
        // By default, show a loading spinner.
        return Padding(
          padding: EdgeInsets.only(top: 20),
          child: Center(
            child: CircularProgressIndicator(),
          ),
        );
      },
    );
  }
}

_getHeroCarosuel(AsyncSnapshot<List> snapshot) {
  var horizontalList = ListView.builder(
    itemCount: snapshot.data.length,
    itemBuilder: (context, index) {
      var each_province = snapshot.data[index];
      return Container(
        child: Padding(
          padding: EdgeInsets.only(bottom: 10, top: 10),
          child: Division(
            style: StyleClass()
              ..width(375)
              ..height(75)
              ..backgroundColor('#ffffff')
              ..borderRadius(all: 20.0)
              ..elevation(50, color: Colors.grey)
              ..align('top')
              ..alignChild('center'),
            child: Container(
              alignment: Alignment.center,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    width: 190,
                    child: Padding(
                      padding: EdgeInsets.only(left: 15, right: 5),
                      child: Text(
                        each_province.name,
                        style: TextStyle(
                            fontSize: 15.0,
                            color: Colors.black,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(right: 5),
                    child: Text(
                      '${(each_province.temp.temp - 273.15).toStringAsFixed(2)}°C',
                      style: TextStyle(
                        fontSize: 15.0,
                        color: Colors.black,
                      ),
                    ),
                  ),
                  Padding(
                      padding: EdgeInsets.only(right: 10),
                      child: Division(
                        style: StyleClass()
                          ..width(50)
                          ..height(50)
                          ..backgroundColor('#53c5d5')
                          ..borderRadius(all: 100.0)
                          ..elevation(0)
                          ..align('center')
                          ..alignChild('center'),
                        child: Image.network(
                          'http://openweathermap.org/img/wn/${each_province.icon.weather[0].icon}.png',
                        ),
                      )),
                ],
              ),
            ),
          ),
        ),
      );
    },
  );

  return new Container(
      width: double.infinity, height: 390.0, child: horizontalList);
}
