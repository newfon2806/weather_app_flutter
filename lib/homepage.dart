import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';

import 'page/current_weather.dart';


class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title, this.primaryColor}) : super(key: key);
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final String title;
  final Color primaryColor;
  final Color lightblue = Color(0xff1f76f9);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  PermissionStatus _status;

  @override
  void initState() {
    super.initState();
    PermissionHandler().checkPermissionStatus(PermissionGroup.locationWhenInUse)
    .then(_updateStatus);
    _askPermission();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: widget._scaffoldKey,
      backgroundColor: widget.primaryColor,
      body: CurrentWeather(),
    );
  }

  void _updateStatus(PermissionStatus status) {
    if(status != _status) {
      setState(() {
        _status = status;
      });
    }
  }

  void _askPermission() {
    PermissionHandler().requestPermissions([PermissionGroup.locationWhenInUse])
    .then(_onStatusRequested);
  }

  void _onStatusRequested(Map<PermissionGroup, PermissionStatus> statuses) {
    final status = statuses[PermissionGroup.locationWhenInUse];
    _updateStatus(status);
  }

}
