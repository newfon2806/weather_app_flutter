import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:division/division.dart';
import 'package:weather_app/assets/iconfont/custom_icon_icons.dart';
import 'package:permission_handler/permission_handler.dart';

import 'current_weather.dart';

class Forecast extends StatefulWidget {
  Forecast({Key key}) : super(key: key);

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  Color primaryColor = Color(0xffe9eae8);
  final Color lightblue = Color(0xff1f76f9);

  @override
  _ForecastState createState() => _ForecastState();
}

class _ForecastState extends State<Forecast> {

  PermissionStatus _status;
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: widget._scaffoldKey,
      appBar: AppBar(
        title: Text(
          "Weather App - Forecast",
          style: TextStyle(color: Colors.black),
        ),
        backgroundColor: widget.primaryColor,
        elevation: 0.0,
        leading: new IconButton(
            color: Colors.black,
            icon: new Icon(CustomIcon.equal),
            onPressed: () => widget._scaffoldKey.currentState.openDrawer()),
      ),
      body: Column(
        children: <Widget>[
          Container(
            alignment: Alignment(-1.0, 0.0),
            child: Padding(
              padding:
                  EdgeInsets.only(left: 20, right: 20, top: 30, bottom: 10),
              child: Text(
                "Forecast in this week",
                style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
              ),
            ),
          ),
          CarouselSlider(
            height: 215.0,
            enlargeCenterPage: true,
            items: [
              "Sunday",
              "Monday",
              "Tuesday",
              "Wednesday",
              "Thurseday",
              "Friday"
            ].map((i) {
              return Builder(
                builder: (BuildContext context) {
                  return Padding(
                    padding: EdgeInsets.only(bottom: 20, top: 10),
                    child: Division(
                      style: StyleClass()
                        ..width(300)
                        // ..height(175)
                        ..backgroundColor('#1f76f9')
                        ..borderRadius(all: 20.0)
                        ..elevation(10)
                        ..align('center')
                        ..alignChild('center'),
                      child: Center(
                        child: Text(
                          '$i',
                          style: TextStyle(fontSize: 20.0, color: Colors.white),
                        ),
                      ),
                    ),
                  );
                },
              );
            }).toList(),
          ),
          Text('$_status'),
        ],
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.only(left: 10, right: 10),
          children: <Widget>[
            DrawerHeader(
              child: Text(
                'Weather App',
                style: TextStyle(color: Colors.black, fontSize: 20),
              ),
              decoration: BoxDecoration(color: widget.primaryColor),
            ),
            ListTile(
              title: Text(
                'Dashboard',
                style: TextStyle(color: Colors.black, fontSize: 15),
              ),
              onTap: () {
                // Update the state of the app
                // ...
                // Then close the drawer
                Navigator.pop(context);
                Navigator.of(context).push(MaterialPageRoute(
                builder: (BuildContext context) => CurrentWeather()));
              },
            ),
            ListTile(
              title: Text(
                'Forecast',
                style: TextStyle(color: Colors.black, fontSize: 15),
              ),
              onTap: () {
                // Update the state of the app
                // ...
                // Then close the drawer
                Navigator.pop(context);
                Navigator.of(context).push(MaterialPageRoute(
                builder: (BuildContext context) => Forecast()));
              },
            ),
          ],
        ),
      ),
    );
  }
}