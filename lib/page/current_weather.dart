import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:weather_app/assets/iconfont/custom_icon_icons.dart';

import '../http_service.dart';
import 'forecast.dart';
import '../get_province_data.dart';

class CurrentWeather extends StatefulWidget {
  CurrentWeather({Key key}) : super(key: key);

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  Color primaryColor = Color(0xffe9eae8);
  final Color lightblue = Color(0xff1f76f9);

  @override
  _CurrentWeatherState createState() => _CurrentWeatherState();
}

class _CurrentWeatherState extends State<CurrentWeather> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: widget._scaffoldKey,
      appBar: AppBar(
        title: Text(
          "Weather App - Current Weather",
          style: TextStyle(color: Colors.black),
        ),
        backgroundColor: widget.primaryColor,
        elevation: 0.0,
        leading: new IconButton(
            color: Colors.black,
            icon: new Icon(CustomIcon.equal),
            onPressed: () => widget._scaffoldKey.currentState.openDrawer()),
      ),
      body: Container(
        // padding: EdgeInsets.only(
        //   left: 30,
        //   right: 30,
        // ),
        alignment: Alignment(0.0, 0.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            GetDataWidget(),
            Padding(
              padding: EdgeInsets.only(top: 10, bottom: 10, left: 30, right: 30),
              child: Text(
                'Today: ${DateFormat("dd MMMM yyyy").format(DateTime.now()).toString()}',
                style: TextStyle(
                    fontSize: 20.0,
                    color: Colors.black,
                    fontWeight: FontWeight.bold),
                textAlign: TextAlign.left,
              ),
            ),
            GetProvinceDataWidget(),
          ],
        ),
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.only(left: 10, right: 10),
          children: <Widget>[
            DrawerHeader(
              child: Text(
                'Weather App',
                style: TextStyle(color: Colors.black, fontSize: 20),
              ),
              decoration: BoxDecoration(color: widget.primaryColor),
            ),
            ListTile(
              title: Text(
                'Dashboard',
                style: TextStyle(color: Colors.black, fontSize: 15),
              ),
              onTap: () {
                // Update the state of the app
                // ...
                // Then close the drawer
                Navigator.pop(context);
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (BuildContext context) => CurrentWeather()));
              },
            ),
            ListTile(
              title: Text(
                'Forecast',
                style: TextStyle(color: Colors.black, fontSize: 15),
              ),
              onTap: () {
                // Update the state of the app
                // ...
                // Then close the drawer
                Navigator.pop(context);
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (BuildContext context) => Forecast()));
              },
            ),
          ],
        ),
      ),
    );
  }
}
