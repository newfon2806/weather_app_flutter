import 'package:flutter/material.dart';
import 'post.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:location/location.dart';
import 'package:division/division.dart';

class HttpService {
  String postsURL;

  final String apiKey = "e5bc2f8dd799d09bc4fddfd07256be57";
  LocationData _locationData;

  HttpService(LocationData _locationData) {
    this._locationData = _locationData;
  }

  Future<Post> getPosts() async {
    postsURL = "https://api.openweathermap.org/data/2.5/weather?lat=" +
        _locationData.latitude.toString() +
        "&lon=" +
        _locationData.longitude.toString() +
        "&appid=" +
        apiKey;
    final http.Response res = await http.get(this.postsURL);

    if (res.statusCode == 200) {
      return Post.fromJson(json.decode(res.body));
    } else {
      print("cannot get data");
      throw Exception('Failed to load album');
    }
  }
}

class GetDataWidget extends StatefulWidget {
  final Location location = new Location();
  static LocationData _locationData;

  _getLocation() async {
    _locationData = await location.getLocation();
  }

  GetDataWidget({Key key}) : super(key: key);
  final HttpService httpService = HttpService(_locationData);

  @override
  _GetDataState createState() => _GetDataState();
}

class _GetDataState extends State<GetDataWidget> {
  Future<Post> futurePost;

  @override
  void initState() {
    super.initState();
    widget._getLocation();
    futurePost = widget.httpService.getPosts();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Post>(
      future: futurePost,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return Padding(
            padding: EdgeInsets.only(bottom: 20, top: 15),
            child: Division(
              style: StyleClass()
                ..width(375)
                ..height(220)
                ..backgroundColor('#1f76f9')
                ..borderRadius(all: 20.0)
                ..elevation(10)
                ..align('top')
                ..alignChild('center'),
              child: Container(
                alignment: Alignment.center,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Image.network(
                      'http://openweathermap.org/img/wn/${snapshot.data.icon.weather[0].icon}@2x.png',
                    ),
                    Padding(
                      padding: EdgeInsets.only(bottom: 5),
                      child: Text(
                        '${(snapshot.data.temp.temp - 273.15).toStringAsFixed(2)}°C',
                        style: TextStyle(
                            fontSize: 35.0,
                            color: Colors.white,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    Text(
                      snapshot.data.name,
                      style: TextStyle(
                          fontSize: 20.0,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    ),
                    Text(
                      '${snapshot.data.icon.weather[0].main} / ${snapshot.data.icon.weather[0].description}',
                      style: TextStyle(fontSize: 15.0, color: Colors.white),
                    ),
                  ],
                ),
              ),
            ),
          );
        } else if (snapshot.hasError) {
          return Text("${snapshot.error}");
        }

        // By default, show a loading spinner.
        return CircularProgressIndicator();
      },
    );
  }
}
